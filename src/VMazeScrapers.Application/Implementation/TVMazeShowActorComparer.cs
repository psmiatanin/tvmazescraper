﻿using System;
using System.Collections.Generic;
using System.Text;
using TVMazeScraper.Persistance.TVMazeDatabase.Contract;

namespace TVMazeScraper.Application.Implementation
{
    internal class TVMazeShowActorEqualityComparer : IEqualityComparer<TVMazeShowActor>
    {
        public bool Equals(TVMazeShowActor x, TVMazeShowActor y)
        {
            if (ReferenceEquals(x, y))
            {
                return true;
            }
            else if (x is null || y is null)
            {
                return false;
            }

            return x.Id == y.Id;
        }

        public int GetHashCode(TVMazeShowActor obj)
        {
            return obj.Id.GetHashCode();
        }
    }
}
