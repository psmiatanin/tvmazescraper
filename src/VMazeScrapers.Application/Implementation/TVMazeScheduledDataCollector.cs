﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using TVMazeScraper.Application.Contract;
using TVMazeScraper.Common;
using TVMazeScraper.Persistance.TVMazeDatabase.Contract;
using TVMazeScraper.Persistance.TVMazeScraperDatabase.Contract;
using TVMazeScraper.Persistance.TVMazeScraperDatabase.Implementation;
using Microsoft.Extensions.Logging;
using System.Net.Http;
using System.Threading;

namespace TVMazeScraper.Application.Implementation
{
    public class TVMazeScheduledDataCollector : ITVMazeScheduledDataCollector
    {
        private readonly ITVMazeDatabaseShowRepository _showRepository;
        private readonly ITVMazeDatabaseShowCastRepository _showCastRepository;
        private readonly Func<TVMazeScraperContext> _scraperDataContextFactory;

        public TVMazeScheduledDataCollector(ITVMazeDatabaseShowRepository showRepository,
            ITVMazeDatabaseShowCastRepository showCastRepository,
            Func<TVMazeScraperContext> scraperDataContextFactory)
        {
            _showRepository = showRepository.EnsureIsNotNull(nameof(showRepository));
            _showCastRepository = showCastRepository.EnsureIsNotNull(nameof(showCastRepository));
            _scraperDataContextFactory = scraperDataContextFactory.EnsureIsNotNull(nameof(scraperDataContextFactory));
        }

        public async Task SyncronizeDataAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            //The method is aimed to synchronize the microservice data with data in TVMaze database (single source of truth)
            //The method should have an analysys logic what shows should be added and and what shows should be updated.
            //The method version tries to override all existing data with new one. It is not proper and optimal logic. The logic 
            //is like stub to demonstrate concept.

            var map = await GrabAllShowsDataAsync();

            IEnumerable<PersonRecord> persons;
            IEnumerable<TVShowRecord> shows;
            BuildModelToBeSaved(map, out persons, out shows);

            using (var context = _scraperDataContextFactory())
            {
                using (var transaction = await context.Database.BeginTransactionAsync(cancellationToken))
                {
                    await context.ClearAllAsync(cancellationToken);

                    await context.Person.AddRangeAsync(persons, cancellationToken);
                    await context.Tvshow.AddRangeAsync(shows, cancellationToken);
                    await context.SaveChangesAsync(cancellationToken);

                    transaction.Commit();
                }
            }
        }

        private void BuildModelToBeSaved(Dictionary<TVMazeShow, TVMazeShowCastItem[]> map,
            out IEnumerable<PersonRecord> persons,
            out IEnumerable<TVShowRecord> shows)
        {
            persons = map.SelectMany(it => it.Value).Select(x => x.Person).Distinct(new TVMazeShowActorEqualityComparer())
                .Select(it => new PersonRecord()
                {
                    Id = it.Id,
                    Name = it.Name,
                    Birthday = it.Birthday
                });

            var tvShows = new List<TVShowRecord>();

            foreach (var it in map)
            {
                var showRecord = new TVShowRecord()
                {
                    Id = it.Key.Id,
                    Name = it.Key.Name,
                };

                foreach (var personId in it.Value.Select(x => x.Person.Id).Distinct())
                {
                    showRecord.TvshowCast.Add(new TVShowCastRecord()
                    {
                        PersonId = personId,
                        ShowId = showRecord.Id
                    });
                }

                tvShows.Add(showRecord);
            }

            shows = tvShows;
        }

        private async Task<Dictionary<TVMazeShow, TVMazeShowCastItem[]>> GrabAllShowsDataAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            //The site uses rate. The logic is aimed to retrive some data (not full data set) => here we have 
            //exception handling + some delay to encrease amount of getting data

            var pageNum = 0;

            var output = new Dictionary<TVMazeShow, TVMazeShowCastItem[]>();

            while (true)
            {
                var shows = await _showRepository.GetShowsAsync(pageNum++);

                if (shows == null || shows.Length == 0)
                {
                    break;
                }
                else
                {
                    foreach (var it in shows)
                    {
                        var cast = await _showCastRepository.GetShowCastAsync(it.Id);
                        output.Add(it, cast);
                    }
                }
            }

            return output;
        }
    }
}
