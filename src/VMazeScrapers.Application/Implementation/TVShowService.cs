﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TVMazeScraper.Application.Contract;
using TVMazeScraper.Persistance.TVMazeScraperDatabase.Implementation;
using TVMazeScraper.Common;
using System.Threading;

namespace TVMazeScraper.Application.Implementation
{
    public class TVShowService : ITVShowService
    {
        private readonly TVMazeScraperContext _dataContext;

        public TVShowService(TVMazeScraperContext dataContext)
        {
            _dataContext = dataContext.EnsureIsNotNull(nameof(dataContext));
        }

        public async Task<TVShow[]> GetAsync(int skip, int take, CancellationToken cancellationToken = default(CancellationToken))
        {
            skip.EnsureIsGreaterThan(nameof(skip), -1);
            take.EnsureIsGreaterThan(nameof(take), 0);

            return await _dataContext.Tvshow
                                    .Include(it => it.TvshowCast)
                                        .ThenInclude(it => it.Person)
                                    .Skip(skip)
                                    .Take(take)
                                    .Select(it => new TVShow()
                                    {
                                        Id = it.Id,
                                        Name = it.Name,
                                        Cast = it.TvshowCast.Select(cast => cast.Person)
                                                            .Select(person => new TVShowCastItem()
                                                            {
                                                                Id = person.Id,
                                                                Name = person.Name,
                                                                Birthday = person.Birthday
                                                            })
                                                            .ToArray()
                                    })
                                    .ToArrayAsync(cancellationToken);
        }
    }
}
