﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TVMazeScraper.Application.Contract
{
    public class TVShowCastItem
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public DateTime? Birthday { get; set; }
    }
}
