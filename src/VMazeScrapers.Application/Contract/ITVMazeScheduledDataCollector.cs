﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TVMazeScraper.Application.Contract
{
    public interface ITVMazeScheduledDataCollector
    {
        Task SyncronizeDataAsync(CancellationToken cancellationToken = default(CancellationToken));
    }
}
