﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TVMazeScraper.Application.Contract
{
    public class TVShow
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public TVShowCastItem[] Cast { get; set; } = new TVShowCastItem[] { };
    }
}
