﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TVMazeScraper.Application.Contract
{
    public interface ITVShowService
    {
        Task<TVShow[]> GetAsync(int skip, int take, CancellationToken cancellationToken = default(CancellationToken));
    }
}
