﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using TVMazeScraper.Application.Contract;
using TVMazeScraper.Application.Implementation;
using TVMazeScraper.Persistance.TVMazeDatabase.Contract;
using TVMazeScraper.Persistance.TVMazeDatabase.Implementation;
using TVMazeScraper.Persistance.TVMazeScraperDatabase.Implementation;

namespace TVMazeScraper.DataCollector.Host
{
    public class Program
    {
        private readonly static int DefaultDataSyncPeriodMilliseconds = 86400000; //=24hrs

        public static void Main(string[] args)
        {
            var hostBuilder = new HostBuilder();

            hostBuilder.ConfigureHostConfiguration(configBuilder =>
            {
                configBuilder.SetBasePath(Directory.GetCurrentDirectory());
                configBuilder.AddJsonFile("hostsettings.json", optional: true);
            });

            hostBuilder.ConfigureAppConfiguration((context, configBuilder) =>
            {
                configBuilder.AddJsonFile("appsettings.json", optional: false);
            });

            hostBuilder.ConfigureServices((context, services) => 
            {
                ConfigureServices(services, context.Configuration);
            });

            hostBuilder.Build().Run();
        }

        private static void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton(serviceProvider => configuration);

            services.AddLogging(loggingBuilder =>
            {
                //TODO: ...
            });

            services.AddTransient(serviceProvider => CreateHttpRequestExecutor(serviceProvider, configuration));

            services.AddHttpClient("TVMazeDb", client => InitializeHttpClient(client, configuration))
                .AddTypedClient<ITVMazeDatabaseShowRepository, TVMazeDatabaseShowRepository>()
                .AddTypedClient<ITVMazeDatabaseShowCastRepository, TVMazeDatabaseShowCastRepository>();

            var sqlConnectionString = configuration["ConnectionStrings:TVMazeScraper"];
            services.AddTransient<Func<TVMazeScraperContext>>(serviceProvider =>
            {
                return () => new TVMazeScraperContext(sqlConnectionString);
            });

            services.AddTransient<ITVMazeScheduledDataCollector, TVMazeScheduledDataCollector>();

            var dataSyncPeriod = configuration.GetValue("DataSyncPeriodMilliseconds", DefaultDataSyncPeriodMilliseconds);
            services.Configure<TVMazeDataCollectorHostedServiceOptions>(options =>
            {
                options.DataSyncPeriod = dataSyncPeriod;
            });

            services.AddSingleton<IHostedService>(serviceProvider =>
            {
                return new TVMazeDataCollectorHostedService(
                    serviceProvider.GetRequiredService<IOptions<TVMazeDataCollectorHostedServiceOptions>>().Value,
                    serviceProvider.GetRequiredService<ITVMazeScheduledDataCollector>(),
                    serviceProvider.GetRequiredService<ILoggerFactory>().CreateLogger<TVMazeDataCollectorHostedService>());
            });
        }

        private static void InitializeHttpClient(HttpClient client, IConfiguration configuration)
        {
            var tvMazeBaseUri = configuration["TVMazeDatabase:BaseAddress"];
            client.BaseAddress = new Uri(tvMazeBaseUri);
        }

        private static IHttpRequestExecutor CreateHttpRequestExecutor(IServiceProvider serviceProvider, IConfiguration configuration)
        {
            var delaysAsString = configuration["TVMazeDatabase:PollyRetryDelays"];
            var delays = delaysAsString.Split(',', StringSplitOptions.RemoveEmptyEntries).Select(it => TimeSpan.FromMilliseconds(int.Parse(it.Trim())));

            var options = new HttpRequestExecutorOptions();
            options.PollyRetryDelays.AddRange(delays);

            return new HttpRequestExecutor(options);
        }
    }
}
