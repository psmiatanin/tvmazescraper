﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TVMazeScraper.Common.Validation;

namespace TVMazeScraper.DataCollector.Host
{
    public sealed class TVMazeDataCollectorHostedServiceOptions : ValidatableObject
    {
        /// <summary>
        /// Data synchronization period in milliseconds. Min value is 1 sec, max value = 24hr
        /// </summary>
        [RangeAttribute(1, 86400000)]
        public int DataSyncPeriod { get; set; }
    }
}
