﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TVMazeScraper.Common;
using TVMazeScraper.Common.Validation;
using TVMazeScraper.Application.Contract;

namespace TVMazeScraper.DataCollector.Host
{
    /// <summary>
    /// The hosted service is aimed to sync TVMaze database data with the microservice data on schedule basis
    /// </summary>
    public sealed class TVMazeDataCollectorHostedService : BackgroundService
    {
        private readonly TVMazeDataCollectorHostedServiceOptions _options;
        private readonly ITVMazeScheduledDataCollector _dataCollector;
        private readonly ILogger _logger;

        public TVMazeDataCollectorHostedService(
            TVMazeDataCollectorHostedServiceOptions options,
            ITVMazeScheduledDataCollector dataCollector, 
            ILogger<TVMazeDataCollectorHostedService> logger)
        {
            _dataCollector = dataCollector.EnsureIsNotNull(nameof(dataCollector));

            _options = options.EnsureIsNotNull(nameof(options));

            if (!DataAnnotationValidator.TryValidate(options))
            {
                throw new ArgumentException("Options have invalid data", nameof(options));
            }

            _logger = logger.EnsureIsNotNull(nameof(logger));
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    await _dataCollector.SyncronizeDataAsync();
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "Data synchronization error");
                    throw;
                }

                await Task.Delay(_options.DataSyncPeriod, stoppingToken);
            }
        }
    }
}
