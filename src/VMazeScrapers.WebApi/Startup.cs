﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Swagger;
using TVMazeScraper.Application.Contract;
using TVMazeScraper.Application.Implementation;
using TVMazeScraper.Persistance.TVMazeScraperDatabase.Implementation;
using TVMazeScraper.WebApi.Code;
using TVMazeScraper.WebApi.Code.Cache;
using VMazeScrapers.WebApi.Code.Filters;

namespace TVMazeScraper.WebApi
{
    public class Startup
    {
        private const string SwaggerDocName = "tvmazescraper";
        private const int DefaultPageItemCount = 250;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(options => 
            {
                options.Filters.Add(new ValidationFilterAttribute());
            });

            services.AddDistributedMemoryCache();
            services.AddTransient<IDistributedApplicationCache, DistributedApplicationCache>();

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc(SwaggerDocName, new Info { Title = "TVMaze Scraper Api", Version = "v1" });
            });

            var pageItemCount = Configuration.GetValue("PageItemCount", DefaultPageItemCount);
            services.Configure<TVShowControllerSettings>(options =>
            {
                options.PageItemCount = pageItemCount;
            });

            var sqlConnectionString = Configuration["ConnectionStrings:TVMazeScraper"];
            services.AddTransient(serviceProvider => new TVMazeScraperContext(sqlConnectionString));

            services.AddScoped<ITVShowService, TVShowService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();

            app.UseSwagger(o => o.RouteTemplate = "api-docs/{documentName}/swagger.json");
            app.UseSwaggerUI(o =>
            {
                o.SwaggerEndpoint($"/api-docs/{SwaggerDocName}/swagger.json", "TvMaze Scraper Web API");
            });
        }
    }
}
