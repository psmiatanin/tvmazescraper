﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TVMazeScraper.Application.Contract;
using TVMazeScraper.Common;
using TVMazeScraper.Common.Validation;
using TVMazeScraper.WebApi.Code;
using TVMazeScraper.WebApi.Model;
using Microsoft.Extensions.Logging;
using TVMazeScraper.WebApi.Code.Cache;

namespace TVMazeScraper.WebApi.Controllers
{
    [Route("api/shows")]
    public class TVShowController : ControllerBase
    {
        private readonly ITVShowService _repository;
        private readonly int _pageItemCount;

        public TVShowController(ITVShowService repository, IOptions<TVShowControllerSettings> settings)
        {
            _repository = repository.EnsureIsNotNull(nameof(repository));

            settings.EnsureIsNotNull(nameof(TVShowControllerSettings));
            if (!DataAnnotationValidator.TryValidate(settings))
            {
                throw new ArgumentException("Invalid controller settings", nameof(settings));
            }

            _pageItemCount = settings.Value.PageItemCount;
        }

        [HttpGet]
        public async Task<TVShow[]> GetAsync(
            [FromServices]IDistributedApplicationCache cache, 
            [FromQuery]TVShowRequestModel model)
        {
            Task<TVShow[]> getDataTask() => _repository.GetAsync(model.Page * _pageItemCount, _pageItemCount);

            return await cache.GetAsync(getDataTask, $"TVSHOWS_CHACHE-PAGE-{model.Page}-[68458EDE89714C1C850577A623DA3701]")
                .ConfigureAwait(false);
        }
    }
}
