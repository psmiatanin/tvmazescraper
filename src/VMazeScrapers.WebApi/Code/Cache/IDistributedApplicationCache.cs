﻿using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TVMazeScraper.WebApi.Code.Cache
{
    public interface IDistributedApplicationCache : IDistributedCache
    {
        Task<TData> GetAsync<TData>(
            Func<Task<TData>> getTaskToGetDataAsync,
            string cacheKey,
            IBiDirectionalDataConverter<string, TData> converter = null,
            DistributedCacheEntryOptions options = null);
    }
}
