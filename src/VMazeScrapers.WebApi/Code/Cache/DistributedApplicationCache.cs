﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;
using TVMazeScraper.Common;

namespace TVMazeScraper.WebApi.Code.Cache
{
    public class DistributedApplicationCache : IDistributedApplicationCache
    {
        private readonly IDistributedCache _cache;

        public DistributedApplicationCache(IDistributedCache cache)
        {
            _cache = cache.EnsureIsNotNull(nameof(cache));
        }

        public byte[] Get(string key)
        {
            return _cache.Get(key);
        }

        public virtual async Task<TData> GetAsync<TData>(Func<Task<TData>> getTaskToGetDataAsync, string cacheKey, IBiDirectionalDataConverter<string, TData> converter = null, DistributedCacheEntryOptions options = null)
        {
            getTaskToGetDataAsync.EnsureIsNotNull(nameof(getTaskToGetDataAsync));
            cacheKey.EnsureIsNotNullOrWhiteSpace(nameof(cacheKey));

            converter = converter ?? new JsonBiDirectionalDataConverter<TData>();

            TData output;

            var cachedData = await _cache.GetStringAsync(cacheKey);

            if (string.IsNullOrEmpty(cachedData))
            {
                output = await getTaskToGetDataAsync();

                Task addToCache = options != null
                    ? _cache.SetStringAsync(cacheKey, converter.Convert(output), options, CancellationToken.None)
                    : _cache.SetStringAsync(cacheKey, converter.Convert(output), CancellationToken.None);

                await addToCache;
            }
            else
            {
                output = converter.Convert(cachedData);
            }

            return output;
        }

        public Task<byte[]> GetAsync(string key, CancellationToken token = default(CancellationToken))
        {
            return _cache.GetAsync(key, token);
        }

        public void Refresh(string key)
        {
            _cache.Refresh(key);
        }

        public Task RefreshAsync(string key, CancellationToken token = default(CancellationToken))
        {
            return _cache.RefreshAsync(key, token);
        }

        public void Remove(string key)
        {
            _cache.Remove(key);
        }

        public Task RemoveAsync(string key, CancellationToken token = default(CancellationToken))
        {
            return _cache.RemoveAsync(key, token);
        }

        public void Set(string key, byte[] value, DistributedCacheEntryOptions options)
        {
            _cache.Set(key, value, options);
        }

        public Task SetAsync(string key, byte[] value, DistributedCacheEntryOptions options, CancellationToken token = default(CancellationToken))
        {
            return _cache.SetAsync(key, value, options, token);
        }
    }
}
