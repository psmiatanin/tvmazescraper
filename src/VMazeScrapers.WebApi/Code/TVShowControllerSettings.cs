﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TVMazeScraper.WebApi.Code
{
    public class TVShowControllerSettings
    {
        [Range(1, 10000)]
        public int PageItemCount { get; set; }
    }
}
