﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TVMazeScraper.WebApi.Code
{
    public interface IBiDirectionalDataConverter<T1, T2>
    {
        T1 Convert(T2 item);
        T2 Convert(T1 item);
    }
}
