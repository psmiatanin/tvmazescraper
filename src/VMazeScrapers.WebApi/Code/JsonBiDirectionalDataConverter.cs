﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TVMazeScraper.WebApi.Code
{
    public class JsonBiDirectionalDataConverter<T> : IBiDirectionalDataConverter<string, T>
    {
        public string Convert(T item)
        {
            return JsonConvert.SerializeObject(item);
        }

        public T Convert(string item)
        {
            return JsonConvert.DeserializeObject<T>(item);
        }
    }
}
