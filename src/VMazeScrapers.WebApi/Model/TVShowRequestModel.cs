﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TVMazeScraper.Common.Validation;

namespace TVMazeScraper.WebApi.Model
{
    public class TVShowRequestModel : ValidatableObject
    {
        [Range(0, int.MaxValue)]
        public int Page { get; set; }
    }
}
