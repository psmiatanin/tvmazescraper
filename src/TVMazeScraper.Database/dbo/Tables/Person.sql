﻿CREATE TABLE [dbo].[Person] (
    [Id]       BIGINT         NOT NULL,
    [Name]     NVARCHAR (100) NOT NULL,
    [Birthday] DATE           NULL,
    CONSTRAINT [PK_Person] PRIMARY KEY CLUSTERED ([Id] ASC)
);

