﻿CREATE TABLE [dbo].[TVShowCast] (
    [ShowId]   BIGINT NOT NULL,
    [PersonId] BIGINT NOT NULL,
    CONSTRAINT [PK_TVShowCast] PRIMARY KEY CLUSTERED ([ShowId] ASC, [PersonId] ASC),
    CONSTRAINT [FK_TVShowCast_Person] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Person] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_TVShowCast_TVShow] FOREIGN KEY ([ShowId]) REFERENCES [dbo].[TVShow] ([Id])
);

