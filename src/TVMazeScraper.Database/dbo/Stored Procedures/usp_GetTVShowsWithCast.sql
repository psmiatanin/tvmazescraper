﻿CREATE PROCEDURE usp_GetTVShowsWithCast
	@skip INT, 
	@take INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tvShowsTable TABLE ([Id] BIGINT, [Name] NVARCHAR(250))

	INSERT INTO @tvShowsTable ([Id], [Name])
		SELECT [Id], [Name] 
		FROM [dbo].[TVShow] 
		ORDER BY [Id]
		OFFSET (@skip) ROWS FETCH NEXT (@take) ROWS ONLY

	SELECT * FROM @tvShowsTable

	SELECT [p].* 
	FROM [dbo].[Person] [p] JOIN [dbo].[TVShowCast] [c] ON [p].[Id] = [c].[PersonId]
	WHERE [c].[ShowId] IN (SELECT [Id] FROM @tvShowsTable)

	SELECT * 
	FROM [dbo].[TVShowCast] [c]
	WHERE [ShowId] IN (SELECT [Id] FROM @tvShowsTable)
END