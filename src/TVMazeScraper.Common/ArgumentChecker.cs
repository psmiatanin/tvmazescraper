﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TVMazeScraper.Common
{
    public static class ArgumentChecker
    {
        public static T EnsureIsNotNull<T>(this T argument, string argumentName, string errorMessage = "Argument must not be null")
            where T : class
        {
            if (argument == null)
            {
                throw new ArgumentNullException(argumentName, errorMessage);
            }

            return argument;
        }

        public static string EnsureIsNotNullOrWhiteSpace(this string argument, string argumentName, string errorMessage = "Argument must not be null or white space string")
        {
            if (string.IsNullOrWhiteSpace(argument))
            {
                throw new ArgumentException(errorMessage, argumentName);
            }

            return argument;
        }

        public static T EnsureIsGreaterThan<T>(this T argument, string argumentName, T other, string errorMessage = "Invalid argument")
            where T : IComparable<T>
        {
            if (argument.CompareTo(other) <= 0)
            {
                throw new ArgumentException(errorMessage, argumentName);
            }

            return argument;
        }
    }
}
