﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TVMazeScraper.Common.Validation
{
    public static class DataAnnotationValidator
    {
        public static void Validate<T>(T it, IServiceProvider serviceProvider = null, IDictionary<object, object> bag = null) 
            where T : class
        {
            var context = new ValidationContext(it, serviceProvider, bag);
            Validator.ValidateObject(it, context, true);
        }

        public static bool TryValidate<T>(T it, ICollection<ValidationResult> collection = null, IServiceProvider serviceProvider = null, IDictionary<object, object> bag = null) 
            where T : class
        {
            collection = collection ?? new List<ValidationResult>();
            var context = new ValidationContext(it, serviceProvider, bag);
            return Validator.TryValidateObject(it, context, collection, true);
        }
    }
}
