﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace TVMazeScraper.Common.Validation
{
    public abstract class ValidatableObject : IValidatableObject
    {
        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext context)
        {
            var errors = new List<ValidationResult>();

            void addError(ValidationResult vr)
            {
                vr.EnsureIsNotNull(nameof(vr));

                if (vr != ValidationResult.Success)
                {
                    errors.Add(vr);
                }
            }

            Validate(context, addError);

            return errors;
        }

        protected virtual void Validate(ValidationContext context, Action<ValidationResult> addError)
        { }
    }
}
