﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using TVMazeScraper.Persistance.TVMazeDatabase.Contract;

namespace TVMazeScraper.Persistance.TVMazeDatabase.Implementation
{
    public class TVMazeDatabaseShowCastRepository : TVMazeDatabaseRepositoryBase, ITVMazeDatabaseShowCastRepository
    {
        public TVMazeDatabaseShowCastRepository(HttpClient client, IHttpRequestExecutor executor) 
            : base(client, executor)
        { }

        public async Task<TVMazeShowCastItem[]> GetShowCastAsync(long showId, CancellationToken cancellationToken = default(CancellationToken))
        {
            var response = await Executor.ExecuteRequest(() => Client.GetAsync($"/shows/{showId}/cast"));

            return await response.Content
                .ReadAsAsync<TVMazeShowCastItem[]>(cancellationToken)
                .ConfigureAwait(false);
        }
    }
}
