﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TVMazeScraper.Common;
using TVMazeScraper.Persistance.TVMazeDatabase.Contract;

namespace TVMazeScraper.Persistance.TVMazeDatabase.Implementation
{
    public abstract class TVMazeDatabaseRepositoryBase
    {
        protected HttpClient Client { get; }
        protected IHttpRequestExecutor Executor { get; }

        protected TVMazeDatabaseRepositoryBase(HttpClient client, IHttpRequestExecutor executor)
        {
            Client = client.EnsureIsNotNull(nameof(client));
            Executor = executor.EnsureIsNotNull(nameof(executor));
        }
    }
}
