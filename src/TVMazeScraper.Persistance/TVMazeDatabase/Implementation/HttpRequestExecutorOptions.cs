﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TVMazeScraper.Persistance.TVMazeDatabase.Implementation
{
    public class HttpRequestExecutorOptions
    {
        public List<TimeSpan> PollyRetryDelays { get; } = new List<TimeSpan>();
    }
}
