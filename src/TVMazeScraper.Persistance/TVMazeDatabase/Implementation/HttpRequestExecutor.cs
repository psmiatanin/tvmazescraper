﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TVMazeScraper.Common;
using TVMazeScraper.Persistance.TVMazeDatabase.Contract;

namespace TVMazeScraper.Persistance.TVMazeDatabase.Implementation
{
    public class HttpRequestExecutor : IHttpRequestExecutor
    {
        private HttpRequestExecutorOptions Options { get; }

        public HttpRequestExecutor(HttpRequestExecutorOptions options)
        {
            Options = options.EnsureIsNotNull(nameof(options));
        }

        public virtual async Task<HttpResponseMessage> ExecuteRequest(Func<Task<HttpResponseMessage>> originRequest, bool ensureSuccessStatusCode = true)
        {
            HttpResponseMessage response = null;

            if (Options.PollyRetryDelays.Count == 0)
            {
                response = await originRequest();
            }
            else
            {
                foreach (var it in Options.PollyRetryDelays)
                {
                    response = await originRequest();

                    if ((int)response.StatusCode != 429)
                    {
                        break;
                    }

                    await Task.Delay(it);
                }
            }

            return ensureSuccessStatusCode ? response.EnsureSuccessStatusCode() : response;
        }
    }
}
