﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TVMazeScraper.Common;
using TVMazeScraper.Persistance.TVMazeDatabase.Contract;

namespace TVMazeScraper.Persistance.TVMazeDatabase.Implementation
{
    public class TVMazeDatabaseShowRepository : TVMazeDatabaseRepositoryBase, ITVMazeDatabaseShowRepository
    {
        public int PageItemCount => 250;

        public TVMazeDatabaseShowRepository(HttpClient client, IHttpRequestExecutor executor)
            : base(client, executor)
        { }

        public async Task<TVMazeShow[]> GetShowsAsync(int pageNum, CancellationToken cancellationToken = default(CancellationToken))
        {
            pageNum.EnsureIsGreaterThan(nameof(pageNum), -1);

            var response = await Executor.ExecuteRequest(() => Client.GetAsync($"/shows?page={pageNum}"));

            return await response.Content
                .ReadAsAsync<TVMazeShow[]>(cancellationToken)
                .ConfigureAwait(false);
        }
    }
}
