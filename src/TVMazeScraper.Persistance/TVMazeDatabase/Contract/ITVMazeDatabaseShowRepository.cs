﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TVMazeScraper.Persistance.TVMazeDatabase.Contract
{
    public interface ITVMazeDatabaseShowRepository
    {
        int PageItemCount { get; }

        Task<TVMazeShow[]> GetShowsAsync(int pageNum, CancellationToken cancellationToken = default(CancellationToken));
    }
}
