﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TVMazeScraper.Persistance.TVMazeDatabase.Contract
{
    public interface ITVMazeDatabaseShowCastRepository
    {
        Task<TVMazeShowCastItem[]> GetShowCastAsync(long showId, CancellationToken cancellationToken = default(CancellationToken));
    }
}
