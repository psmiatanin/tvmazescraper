﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TVMazeScraper.Persistance.TVMazeDatabase.Contract
{
    public class TVMazeShowCastItem
    {
        public TVMazeShowActor Person { get; set; }

        public TVShowMazeCharacter Character { get; set; }
    }
}
