﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TVMazeScraper.Persistance.TVMazeDatabase.Contract
{
    public class TVShowMazeCharacter
    {
        public long Id { get; set; }

        public string Name { get; set; }
    }
}
