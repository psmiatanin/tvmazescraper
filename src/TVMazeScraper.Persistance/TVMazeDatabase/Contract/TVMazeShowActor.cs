﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TVMazeScraper.Persistance.TVMazeDatabase.Contract
{
    public class TVMazeShowActor
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public DateTime? Birthday { get; set; }


    }
}
