﻿using System;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using TVMazeScraper.Common;
using TVMazeScraper.Persistance.TVMazeScraperDatabase.Contract;

namespace TVMazeScraper.Persistance.TVMazeScraperDatabase.Implementation
{
    public partial class TVMazeScraperContext : DbContext
    {
        private readonly string _connectionString;

        public TVMazeScraperContext(string connectionString)
        {
            _connectionString = connectionString.EnsureIsNotNullOrWhiteSpace(nameof(connectionString));
        }

        public virtual DbSet<PersonRecord> Person { get; set; }
        public virtual DbSet<TVShowRecord> Tvshow { get; set; }
        public virtual DbSet<TVShowCastRecord> TvshowCast { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PersonRecord>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Birthday).HasColumnType("date");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<TVShowRecord>(entity =>
            {
                entity.ToTable("TVShow");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<TVShowCastRecord>(entity =>
            {
                entity.HasKey(e => new { e.ShowId, e.PersonId });

                entity.ToTable("TVShowCast");

                entity.HasOne(d => d.Person)
                    .WithMany(p => p.TvshowCast)
                    .HasForeignKey(d => d.PersonId)
                    .HasConstraintName("FK_TVShowCast_Person");

                entity.HasOne(d => d.Show)
                    .WithMany(p => p.TvshowCast)
                    .HasForeignKey(d => d.ShowId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TVShowCast_TVShow");
            });
        }

        public virtual Task ClearAllAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            return this.Database.ExecuteSqlCommandAsync("EXEC [dbo].[usp_Clear]", cancellationToken);
        }
    }
}
