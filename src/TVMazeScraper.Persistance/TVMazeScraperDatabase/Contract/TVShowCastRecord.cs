﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TVMazeScraper.Persistance.TVMazeScraperDatabase.Contract
{
    public partial class TVShowCastRecord
    {
        public long ShowId { get; set; }

        public long PersonId { get; set; }

        public PersonRecord Person { get; set; }

        public TVShowRecord Show { get; set; }
    }
}
