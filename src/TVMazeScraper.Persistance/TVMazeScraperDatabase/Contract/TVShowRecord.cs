﻿using System;
using System.Collections.Generic;

namespace TVMazeScraper.Persistance.TVMazeScraperDatabase.Contract
{
    public partial class TVShowRecord
    {
        public TVShowRecord()
        {
            TvshowCast = new HashSet<TVShowCastRecord>();
        }

        public long Id { get; set; }

        public string Name { get; set; }

        public ICollection<TVShowCastRecord> TvshowCast { get; set; }
    }
}
