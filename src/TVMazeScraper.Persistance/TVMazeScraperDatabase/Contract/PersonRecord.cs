﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TVMazeScraper.Persistance.TVMazeScraperDatabase.Contract
{
    public partial class PersonRecord
    {
        public PersonRecord()
        {
            TvshowCast = new HashSet<TVShowCastRecord>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public DateTime? Birthday { get; set; }

        public ICollection<TVShowCastRecord> TvshowCast { get; set; }
    }
}
