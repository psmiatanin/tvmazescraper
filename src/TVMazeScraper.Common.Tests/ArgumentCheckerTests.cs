﻿using FluentAssertions;
using NUnit.Framework;
using System;

namespace TVMazeScraper.Common.Tests
{
    [TestFixture]
    public class ArgumentCheckerTests
    {
        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        [TestCase("\t")]
        [TestCase("\r\n")]
        public void EnsureIsNotNullOrWhiteSpace_ArgumentIsNullOrWhiteSpace_ArgumentException(string argument)
        {
            Action action = () => argument.EnsureIsNotNullOrWhiteSpace("argName");
            action.Invoking(it => it())
                .Should().Throw<ArgumentException>()
                .And.ParamName.Should().Be("argName");
        }

        [Test]
        public void EnsureIsNotNullOrWhiteSpace_ArgumentIsNotNullOrWhiteSpace_ReturnsArgument()
        {
            var it = "Hello word";
            it.EnsureIsNotNullOrWhiteSpace("argName").Should().Be(it);
        }

        [Test]
        public void EnsureIsNotNull_ArgumentIsNull_ArgumentNullException()
        {
            Action action = () => ((object)null).EnsureIsNotNull("argument", "It is error message");
            action.Invoking(it => it())
                .Should().Throw<ArgumentNullException>()
                .And.ParamName.Should().Be("argument");
        }

        [Test]
        public void EnsureIsNotNull_NullableArgumentIsNull_ArgumentNullException()
        {
            int? item = null;
            Action action = () => ((object)item).EnsureIsNotNull("argument", "It is error message");

            action.Invoking(it => it())
                .Should().Throw<ArgumentNullException>()
                .And.ParamName.Should().Be("argument");
        }

        [Test]
        public void EnsureIsNotNull_ArgumentIsNotNull_ReturnsArgument()
        {
            var it = new object();
            it.EnsureIsNotNull("argument", "It is error message").Should().Be(it);
        }

        [TestCase(4, 5)]
        [TestCase(10, 10)]
        [TestCase(4.1, 5.8)]
        [TestCase("Audi", "BMW")]
        public void EnsureIsGreaterThan_ArgumentIsLessOrEqualThanTested_ArgumentException<T>(T v1, T v2)
            where T : IComparable<T>
        {
            Action action = () => ArgumentChecker.EnsureIsGreaterThan(v1, nameof(v1), v2);
            action.Invoking(e => e()).Should().Throw<ArgumentException>();
        }

        [TestCase(40, 5)]
        [TestCase(10.1, 10.0)]
        [TestCase(42.1, 5.8)]
        [TestCase("BMW", "Audi")]
        public void EnsureIsGreaterThan_ArgumentIsMoreThanTested_ReturnsArgument<T>(T v1, T v2)
            where T : IComparable<T>
        {
            ArgumentChecker.EnsureIsGreaterThan(v1, nameof(v1), v2).Should().Be(v1);
        }
    }
}
