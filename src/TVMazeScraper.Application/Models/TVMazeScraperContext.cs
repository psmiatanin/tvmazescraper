﻿using System;
using System.Data.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using TVMazeScraper.Common;

namespace TVMazeScraper.Application.Models
{
    public partial class TVMazeScraperContext : DbContext
    {
        private readonly DbConnection _connection;

        public TVMazeScraperContext(DbConnection connection)
        {
            _connection = connection.EnsureIsNotNull(nameof(connection));
        }

        public virtual DbSet<TVShow> TVShows { get; set; }

        public virtual DbSet<TVShowCastItem> TVShowCastItems { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_connection);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TVShow>(entity =>
            {
                entity.ToTable("TVShow");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<TVShowCastItem>(entity =>
            {
                entity.ToTable("TVShowCastItem");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Birthday).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.HasOne(d => d.Show)
                    .WithMany(p => p.TvshowCastItem)
                    .HasForeignKey(d => d.ShowId)
                    .HasConstraintName("FK_TVShowCastItem_TVShow");
            });
        }
    }
}
