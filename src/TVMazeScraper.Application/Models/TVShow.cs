﻿using System;
using System.Collections.Generic;

namespace TVMazeScraper.Application.Models
{
    public partial class TVShow
    {
        public TVShow()
        {
            TvshowCastItem = new HashSet<TVShowCastItem>();
        }

        public long Id { get; set; }
        public string Name { get; set; }

        public ICollection<TVShowCastItem> TvshowCastItem { get; set; }
    }
}
