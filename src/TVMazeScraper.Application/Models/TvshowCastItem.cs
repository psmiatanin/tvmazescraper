﻿using System;
using System.Collections.Generic;

namespace TVMazeScraper.Application.Models
{
    public partial class TVShowCastItem
    {
        public long Id { get; set; }
        public long ShowId { get; set; }
        public string Name { get; set; }
        public DateTime? Birthday { get; set; }

        public TVShow Show { get; set; }
    }
}
