﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using TVMazeScraper.Application.Contract;
using TVMazeScraper.Application.Dependency.Contract.TVMazeDatabase;
using TVMazeScraper.Application.Models;
using TVMazeScraper.Common;
using Microsoft.EntityFrameworkCore;
using System.Data.Common;
using System.Data.SqlClient;

namespace TVMazeScraper.Application
{
    public class TVMazeDataCollector : ITVMazeDataCollector
    {
        private readonly ITVMazeDatabaseShowRepository _showRepository;
        private readonly ITVMazeDatabaseShowCastRepository _showCastRepository;

        public TVMazeDataCollector(ITVMazeDatabaseShowRepository showRepository,
            ITVMazeDatabaseShowCastRepository showCastRepository)
        {
            _showRepository = showRepository.EnsureIsNotNull(nameof(showRepository));
            _showCastRepository = showCastRepository.EnsureIsNotNull(nameof(showCastRepository));
        }

        public async Task SyncronizeDataAsync()
        {
            var getShowsTask = _showRepository.GetShowsAsync();

            using (var context = CreateContext())
            {
                using (var transaction = await context.Database.BeginTransactionAsync())
                {
                    var id = context.TVShows.Max(it => (long?)it.Id);

                    IEnumerable<TVMazeShow> shows = await getShowsTask;

                    var showsToBeAdded = id.HasValue 
                        ? shows.Where(it => it.Id > id.Value)
                        : shows;

                    foreach (var show in showsToBeAdded)
                    {
                        var entity = new Models.TVShow()
                        {
                            Id = show.Id,
                            Name = show.Name,
                        };
                        context.TVShows.Add(entity);

                        var cast = await _showCastRepository.GetShowCastAsync(show.Id);

                        foreach (var castItem in cast)
                        {
                            var castItemEntity = new Models.TVShowCastItem()
                            {
                                Id = castItem.Person.Id,
                                Name = castItem.Person.Name,
                                Birthday = castItem.Person.Birthday,
                                ShowId = show.Id
                            };
                            context.TVShowCastItems.Add(castItemEntity);
                        }
                    }

                    context.SaveChanges();

                    transaction.Commit();
                }
            }
        }

        private TVMazeScraperContext CreateContext()
        {
            var connection = new SqlConnection(@"Data Source=.\SQLEXPRESS2017;Initial Catalog=TVMazeScraper;Integrated Security=True;Persist Security Info=False;Pooling=False;MultipleActiveResultSets=False;Connect Timeout=60;Encrypt=False;TrustServerCertificate=True");
            return new TVMazeScraperContext(connection);
        }
    }
}
