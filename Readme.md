The microservice consists of 2 host processes:
1. Web host. 
It is web host in terms of ASP.NET Core. The host is aimed to expose REST Web API to communicate with outside world
2. Data sync host. 
It is generic host in terms of ASP.NET Core. It is targeted to synchronize data with main TV Maze database. The microservice should work independently. 
In order to follow AP (CAP theorem) the microservice should have its own database and if for some reasom TV Maze database is down this microservice can work.
Eventual consistency is used and the host is aimed to reach data consistency for TVMaze database and TV Maze Scraper

How to run it
1. Setup DB.
The solution has TVMazeScraper.Database database project. Publish it. Point out correct connection strings to the database in both projects *.WebApi and *.Host
2. Collect data from TVMaze database to the TVMaze Scraper microservice
Set *.Host as active project and run it. The project start data collection. The logic does not implement rate consideration or authentication and for that reason the process will collect data until get http exception with 429 code (too many requests). It does not matter. The main point is to get any data we can provide via Web API
3. Start *.WebApi host and make web api call
Set *.WebApi as active project and run it. Browser will be launched and Swagger UI page will be displayed. Exec Web API call